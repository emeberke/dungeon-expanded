var Dungeon = function(canvas) {
    this.engine = new BABYLON.Engine(canvas, true);

    // Resize window event
    var _this = this;
    window.addEventListener("resize", function() {
        _this.engine.resize();
    });

    // Load scene
    BABYLON.SceneLoader.Load("assets/", "dungeon.babylon", this.engine, function(scene) {
        _this.scene = scene;
        _this.initScene();

        scene.executeWhenReady(function() {
            _this.engine.runRenderLoop(function () {
                _this.scene.render();
            });
        });

    });
};

Dungeon.prototype.initScene = function() {
    // GUI
    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    var instructions = new BABYLON.GUI.TextBlock();
    instructions.text = "Move w/ WASD keys, E to hide/show instructions, look with the mouse. Click around to find things.";
    instructions.color = "white";
    instructions.fontSize = 16;
    instructions.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
    instructions.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_BOTTOM
    advancedTexture.addControl(instructions);

    // STEPS
    var step1 = new BABYLON.Sound("step1", "assets/sounds/step1.wav", this.scene);
    var step2 = new BABYLON.Sound("step2", "assets/sounds/step2.wav", this.scene);
    var step3 = new BABYLON.Sound("step3", "assets/sounds/step3.wav", this.scene);

    var goToStep2 = false;

    var rightPressed, leftPressed, upPressed, downPressed, ePressed = false;
    this.scene.actionManager = new BABYLON.ActionManager(this.scene);
    this.scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
        if(evt.sourceEvent.keyCode == 39 || evt.sourceEvent.keyCode == 68) {
            rightPressed = true;
                if (!step1.isPlaying && !step2.isPlaying && !step3.isPlaying) {
                    if (Math.random() < 0.2) {
                        step3.play();
                    } else if (!goToStep2) {
                        step1.play();
                    } else {
                        step2.play();
                    }
                }
        }
        else if(evt.sourceEvent.keyCode == 37 || evt.sourceEvent.keyCode == 65) {
            leftPressed = true;
            if (!step1.isPlaying && !step2.isPlaying && !step3.isPlaying) {
                if (Math.random() < 0.2) {
                    step3.play();
                } else if (!goToStep2) {
                    step1.play();
                } else {
                    step2.play();
                }
            }
        }
        if(evt.sourceEvent.keyCode == 40 || evt.sourceEvent.keyCode == 83) {
            downPressed = true;
            if (!step1.isPlaying && !step2.isPlaying && !step3.isPlaying) {
                if (Math.random() < 0.2) {
                    step3.play();
                } else if (!goToStep2) {
                    step1.play();
                } else {
                    step2.play();
                }
            }
        }
        else if(evt.sourceEvent.keyCode == 38 || evt.sourceEvent.keyCode == 87) {
            upPressed = true;
            if (!step1.isPlaying && !step2.isPlaying && !step3.isPlaying) {
                if (Math.random() < 0.2) {
                    step3.play();
                } else if (!goToStep2) {
                    step1.play();
                } else {
                    step2.play();
                }
            }
        }
        if(evt.sourceEvent.keyCode == 69) {
            ePressed = !ePressed;
            if(ePressed == true) {
                instructions.alpha = 0;
            }
            else
                instructions.alpha = 1;
        }
        step1.onended = function() {
            goToStep2 = true;
        };
        step2.onended = function() {
            goToStep2 = false;
        };
        step3.onended = function() {
            goToStep2 = false;
        };
    }));
    this.scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {
        if(evt.sourceEvent.keyCode == 39 || evt.sourceEvent.keyCode == 68) {
            rightPressed = false;
        }
        else if(evt.sourceEvent.keyCode == 37 || evt.sourceEvent.keyCode == 65) {
            leftPressed = false;
        }
        if(evt.sourceEvent.keyCode == 40 || evt.sourceEvent.keyCode == 83) {
            downPressed = false;
        }
        else if(evt.sourceEvent.keyCode == 38 || evt.sourceEvent.keyCode == 87) {
            upPressed = false;
        }
    }));
    var thinShroom = this.scene.getMeshByName("shroom_thin");
    var thinShroom2 = this.scene.getMeshByName("shroom_thin02");
    var thinShroom3 = this.scene.getMeshByName("shroom_thin003");
    var thinShroom4 = this.scene.getMeshByName("shroom_thin004");
    var thinShroom5 = this.scene.getMeshByName("shroom_thin01");
    var bigShroom = this.scene.getMeshByName("shroom_big01");
    var bigShroom2 = this.scene.getMeshByName("shroom_big003");
    var door = this.scene.getMeshByName("door001");
    var torch1 = this.scene.getMeshByName("torch");
    var torch2 = this.scene.getMeshByName("torch01");
    var light1on = true;
    var light2on = true;
    torch1.isPickable = true;
    torch2.isPickable = true;
    bigShroom2.isPickable = true;

    torch1.actionManager = new BABYLON.ActionManager(this.scene);
    torch2.actionManager = new BABYLON.ActionManager(this.scene);
    bigShroom.actionManager = new BABYLON.ActionManager(this.scene);
    bigShroom2.actionManager = new BABYLON.ActionManager(this.scene);
    thinShroom4.actionManager = new BABYLON.ActionManager(this.scene);
    door.actionManager = new BABYLON.ActionManager(this.scene);

    bigShroom.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {
        instructions.text = "You found a shroom.";
        instructions.alpha = 1;
        bigShroom.position = [0,0,0];
    }));
    thinShroom4.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {
        instructions.text = "Hope these aren't poisonous."
        instructions.alpha = 1;
        thinShroom5.position = [-6,-6,-6];
        thinShroom4.position = [-1,-1,-1];
        thinShroom4.position = [-2,-2,-2];
        thinShroom3.position = [-3,-3,-3];
        thinShroom2.position = [-4,-4,-4];
        thinShroom.position = [-5,-5,-5];
    }));
    door.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {
        instructions.text = "The door is locked. Not much to see here."
        instructions.alpha = 1;
    }));
    bigShroom2.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {
        instructions.text = "Huh. Stubborn shroom."
        instructions.alpha = 1;
    }));

    // Camera attached to the canvas
    var camera = this.scene.activeCamera;
    camera.attachControl(this.engine.getRenderingCanvas());
    camera.keysUp.push(87);
    camera.keysDown.push(83);
    camera.keysLeft.push(65);
    camera.keysRight.push(68);

    this.scene.lights.forEach(function(l) {
        l.dispose();
    });

    var randomNumber = function (min, max) {
        if (min === max) {
            return (min);
        }
        var random = Math.random();
        return ((random * (max - min)) + min);
    };

    // Hemispheric light to light the scene
    var h = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0,1,0), this.scene);
    h.intensity = 0.2;

    var pl1 = new BABYLON.PointLight("pl1", torch1.position, this.scene);
    var pl2 = new BABYLON.PointLight("pl2", torch2.position, this.scene);
    pl1.intensity = pl2.intensity = 0.5;
    pl1.diffuse = pl2.diffuse = BABYLON.Color3.FromInts(255, 123, 63);
    pl1.range = pl2.range = 30;

    var positive = true;
    var di = randomNumber(0, 0.05);
    setInterval(function() {
       if (positive) {
           di *= -1;
       }  else {
           di = randomNumber(0, 0.05);
       }
        positive = !positive;
        pl1.intensity += di;
        pl2.intensity += di;

    }, 50);

    var particleSystem = new BABYLON.ParticleSystem("particles", 2000, this.scene);

    //Texture of each particle
    particleSystem.particleTexture = new BABYLON.Texture("particles/flame.png", this.scene);

    particleSystem.emitter = torch1;
    particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD;
    particleSystem.minSize = 0.8;
    particleSystem.maxSize = 1.2;
    particleSystem.minLifeTime = 0.3;
    particleSystem.maxLifeTime = 1.5;
    particleSystem.minEmitBox = new BABYLON.Vector3(-0.1, 0, -0.1); // Starting all from
    particleSystem.maxEmitBox = new BABYLON.Vector3(0.1, 0.1, 0.1); // To...
    particleSystem.emitRate = 75;
    particleSystem.start();

    var ps2 = particleSystem.clone();
    ps2.emitter = torch2;
    ps2.start();

    // FIRE
    var fire = new BABYLON.Sound("fire", "assets/sounds/fire1.wav", this.scene,
    null, { loop: true, autoplay: true, spatialSound: true, maxDistance: 20, volume:0.2 });
    var fire2 = new BABYLON.Sound("fire2", "assets/sounds/fire2.wav", this.scene,
    null, { loop: true, autoplay: true, spatialSound: true, maxDistance: 20, volume:0.2 });

    fire.setPosition(torch1.position);
    fire2.setPosition(torch2.position);

    torch1.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {
        if(light1on) {
            pl1.setEnabled(false);
            particleSystem.stop();
            instructions.text = "Its getting dark.";
            instructions.alpha = 1;
            fire.pause();
        }
        else {
            pl1.setEnabled(true);
            particleSystem.start();
            instructions.text = "Yeah, that's better."
            instructions.alpha = 1;
            fire.play();
        }
        light1on = !light1on;
    }));
    torch2.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, function() {    
        if(light2on) {
            pl2.setEnabled(false);
            ps2.stop();
            instructions.text = "Woah. You like the dark?"
            instructions.alpha = 1;
            fire2.pause();
        }
        else {
            pl2.setEnabled(true);
            ps2.start();
            instructions.text = "Really livens up the place, don't you think?"
            instructions.alpha = 1;
            fire2.play();
        }
       light2on = !light2on; 
    }));

    this.initShadows();

    this.initCollisions();

};

Dungeon.prototype.initShadows = function() {

    this.scene.meshes.forEach(function(mesh) {
        if (mesh.name.indexOf("floor") != -1) {
            mesh.receiveShadows = true;
        }
    });

    var dl = new BABYLON.DirectionalLight("light", new BABYLON.Vector3(0, -0.5, -0.3), this.scene);
    dl.intensity = 0.5;
    var generator = new BABYLON.ShadowGenerator(512, dl);

    this.scene.meshes.forEach(function(mesh) {
        if (mesh.name.indexOf("shadow") != -1) {
            generator.getShadowMap().renderList.push(mesh);
        }
    });
    generator.useBlurVarianceShadowMap = true;

};

Dungeon.prototype.initCollisions = function() {

    this.scene.collisionsEnabled = true;

    this.scene.meshes.forEach(function(mesh) {
        if (mesh.name.indexOf("collider") != -1) {
            mesh.isVisible = false;
        }
    });
};